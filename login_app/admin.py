from django.contrib import admin
from .models import RequestPasswordReset

# Register your models here.

admin.site.register(RequestPasswordReset)
