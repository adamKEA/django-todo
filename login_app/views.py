from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from .models import RequestPasswordReset
from django.urls import reverse


def login(request):
    context = {}

    if request.method == 'POST':
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user:
            dj_login(request, user)
            return redirect('todo_app:index')
        else:
            context = {
                'error': 'Bad user name or password'
            }
    return render(request, 'login_app/login.html', context)

def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse(request, 'login_app/login.html'))

def sign_up(request):
    context = {}
    if request.method == 'POST':
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        username = request.POST['username']
        email = request.POST['email']
        if password == confirm_password:
            if User.objects.create_user(username, email, password):
                return HttpResponseRedirect(reverse('login_app:login'))
            else:
                context = {
                    'error': 'Could not create account, try again'
                }
        else:
            context = {
                'error': 'Passwords did not match, try again'
            }
    return render(request, 'login_app/sign_up.html', context)

def delete_account(request):
    pass

def request_password_reset(request):
    if request.method == 'POST':
        post_user = request.POST['username']
        user = None

        if post_user:
            try:
                user = User.objects.get(username=post_user)
            except:
                print(f'invalid password request: {post_user}')
        else:
            post_user = request.POST['email']
            try:
                user = User.objects.get(email=post_user)
            except:
                print(f'invalid password request: {post_user}')

        if user:
            prr = RequestPasswordReset()
            prr.user = user
            prr.save()
            print(prr)
            return redirect('login_app:password_reset')
    return render(request, 'login_app/request_password_reset.html')

def password_reset(request):
    if request.method == 'POST':
        post_user = request.POST['username']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        token = request.POST['token']

        if password == confirm_password:
            try:
                prr = RequestPasswordReset().objects.get(token=token)
                prr.save()
            except:
                print('Invalid password reset attempt')
                return render(request, 'login_app/password_reset.html')

            user = prr.user
            user.set_password(password)
            user.save()
            return HttpResponseRedirect(reverse('login_app:login'))

    return render(request, 'login_app/password_reset.html')
