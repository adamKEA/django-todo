from django.db import models
from django.contrib.auth.models import User
from secrets import token_urlsafe

# Create your models here.

class RequestPasswordReset(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=23, default=token_urlsafe)
    create_timestamp = models.DateTimeField(auto_now_add=True)
    updated_timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user} - {self.token}'
